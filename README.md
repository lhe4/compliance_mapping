### Dependency
Other than the dependencies in requirements.txt, we also need a open source library pdftotree. Unfortunately the project has been inactive so we have to check it out to local, fix some dependency issues and install locally.
Project link:
https://github.com/HazyResearch/pdftotree
Dependency fix:
https://github.com/HazyResearch/pdftotree/pull/127/files
Install:
```
pip install -e <the path of pdftotree project>
```

### Create new standard:
```
python src/main.py create-standard --version=3.0.0 --type=cis_benchmark --name=cis_microsoft_365_benchmark --platforms=Microsoft --display-name="CIS Microsoft 365 Foundation Benchmark V3.0.0"
```
### Create mappings:
Currently it only creates rules mappings.
```
python src/main.py create-mappings --standard cis_microsoft_365_benchmark --standard-version=3.0.0
```