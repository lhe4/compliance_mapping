import requests
import json

PUBLISH_URL = 'https://qeportalapi.in.qe.obsec.us/api'

def get_standard(name, version):
    url = f"{PUBLISH_URL}/getPostureStandard?name={name}&version={version}"
    response = requests.get(url)
    if response.status_code == 200:
        return True, response.text
    elif response.status_code == 404:
        return False, f'Standard does not exist: {name}, {version}'
    else:
        message = json.loads(response.text)
        return False, f'Failed to search standard: {name}, {version}. {message}'

def save_settings(settings):
    url = f"{PUBLISH_URL}/savePostureSettings"
    response = requests.post(url, json=json.dumps({"settings": settings}))
    return response.text

def load_settings(platform):
    url = f"{PUBLISH_URL}/getPostureSettings?platform={platform}"
    response = requests.get(url)
    return json.loads(response.text)

def create_update_standard(name, version, type, platform, display_name=None, update=False):
    if not display_name:
        display_name = name
    payload_dict = {
        "name": name,
        "version": version,
        "type": type,
        "platform": platform,
        "display_name": display_name,
        "update": update
    }
    

    url = f"{PUBLISH_URL}/saveStandard"
    headers = {
        "Content-Type": "application/json"
    }

    response = requests.post(url, json=json.dumps(payload_dict), headers=headers)
    if response.status_code == 200:
        return True, None
    else:
        message = json.loads(response.text)
        return False, f'Create new standard failed: {response.status_code}, {message["msg"]}'
