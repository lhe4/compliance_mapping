import csv
import json
import re
import requests

LLM_URL = "http://localhost:9000/aws-anthropic-kb/invoke"
QE_PORTAL = "http://localhost:8000"
MAPPING_FILE_PATH = "output/maping.json"

def load_obsidian_settings(saveCsv=False):
    settings = []
    with open("files/raw_settings.json") as f:
        load_settings = json.load(f)
    for r in load_settings.get("data").get("listPostureSettings").get("data"):
        uuid_pattern = re.compile(r'^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$')
        if not uuid_pattern.match(r.get("platform_id")):
            settings.append({
                "setting_id": r.get("id"),
                "name": r.get("name"),
                "description": r.get("description"),
                "platform": r.get("platform_id")
                })
    if saveCsv:
        with open('output/settings.csv', 'w', newline='', encoding='utf-8') as file:
            writer = csv.DictWriter(file, fieldnames=settings[0].keys())
            writer.writeheader()
            writer.writerows(settings)
    return settings

def load_controls(saveCsv=False):
    with open('output/controls.json') as f:
        controls = json.load(f)
    if saveCsv:
        with open('output/controls.csv', 'w', newline='', encoding='utf-8') as file:
            writer = csv.DictWriter(file, fieldnames=controls[0].keys())
            writer.writeheader()
            writer.writerows(controls)
    return controls

def load_control_rule_list(platform_id):
    with open('output/controls.json') as f:
        controls = json.load(f)
    with open("files/rules.json") as f:
        rules = json.load(f)
    filter_rules = []
    for r in rules.get("data").get("listGlobalPostureRules").get("rules"):
        if r.get("platform_id") == platform_id and r.get("obsidian_rule"):
            filter_rules.append({
                "rule_id": r.get("rule_id"),
                "name": r.get("name"),
                "description": r.get("description"),
                })
    # with open(f'files/{platform_id}_rules.json') as f:
    #     rules = json.load(f)
    return controls, filter_rules

def get_ai_rule_mapping_message(controls, rules):
    with open('output/mappings.csv', 'a', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        for rule in rules:
            prompt = f"With security rule {rule['rule_id']} specified by Obsidian Security, find some related recommendation IDs in CIS Microsoft 365 Foundataions Benchmark 3.0.0. Note the recommendation IDs are in the format of 1.1, 1.1.2 and etc."
            response = requests.post(LLM_URL, json={"input_data": prompt})
            output = json.loads(response.text)
            csvwriter.writerow([rule['rule_id'], output['generated_text']])

def map_settings():
    with open('output/settings_mappings.csv', 'a', newline='') as csvfile:
        with open(f"files/raw_settings.json", 'r') as f:
            setting_json = json.load(f)
        csvwriter = csv.writer(csvfile)
        for setting in setting_json['data']['listPostureSettings']['data']:
            print(setting['id'])
            if setting['platform_id'] == 'microsoft':
                                    
                prompt = f"Explain Obsidian Security specified setting '{setting['id']}', find highly relevant control specified by Obsidian Security from CIS Microsoft Admin Foundations Benchmark V3.0.0. Each Obsidian Security control has a ref_id. Return the ref_id in the result, and explain why you choose this. Also return a confidence index (from 1 to 100) for your choice."
                response = requests.post(LLM_URL, json={"input_data": prompt})
                output = json.loads(response.text)
                csvwriter.writerow([setting['id'], output])

def get_ai_setting_mapping_message(controls, settings):
    
    prompt = "From a list of controls and a list of settings provided below, find all highly relevant settings to each control. If no relevant settings found, return No Settings Found. Mapped settings must be in the provided list.\n\n"
    prompt += "\nControls:\n"
    for control in controls:
        prompt += f"{control['ref_id']} - {control['name']} - {control['description']}\n"
    prompt += "\nSettings:\n"
    for s in settings:
        # id = s['setting_id'].replace('-', '_')
        description = re.sub(r'\n+', ' | ', s['description'])
        prompt += f"Setting id: {s['id']} - Setting name: {s['name']} - Setting description: {description}\n"
    response = requests.post(LLM_URL, json={"input_data": prompt})
    with open(MAPPING_FILE_PATH, 'w') as file:
        file.write(response.text)

def parse_mappings():
    mapping_dict = {}
    with open(MAPPING_FILE_PATH) as f:
        json_msg = json.load(f)['generated_text']
    mapping_strs = json_msg.split("\n\n")
    current_control = None
    rule_list = []
    mapping_dict = {}

    CONTROL_PATTERN = re.compile(r'^For control (\d+(\.\d+)*)')
    for mp_str in mapping_strs:
        mp_str = mp_str.strip()
        if CONTROL_PATTERN.match(mp_str):
            if current_control is not None:
                mapping_dict[current_control] = rule_list
                rule_list = []
            control_id = CONTROL_PATTERN.match(mp_str)[1]
            current_control = control_id
            rule_str_list = mp_str.split("\n")
            for rule_str in rule_str_list:
                rule_str = rule_str.strip()
                if rule_str.startswith("-"):
                    rule_str = rule_str.lstrip("- ")
                    if rule_str.startswith("No highly relevant rules"):
                        break
                    else:
                        rule_list.append(rule_str)
    if current_control:
        mapping_dict[current_control] = rule_list
    for control in mapping_dict:
        print(control)
        for rule in mapping_dict[control]:
            print(f"  - {rule}")
    url = f"{QE_PORTAL}/api/savePostureRuleMappings"
    response = requests.post(url, json=json.dumps(mapping_dict))
    print(response.text)


def import_rules(rules):
    for rule in rules:
        rule['platform'] = 'google'
    url = f"{QE_PORTAL}/api/savePostureRules"
    response = requests.post(url, json=json.dumps({"rules": rules}))
    print(response.text)
# controls, rules = load_control_rule_list("google")
# get_ai_mapping_message()
# parse_mappings()
# import_rules()
load_obsidian_settings(saveCsv=True)