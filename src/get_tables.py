import tabula
PDF_PATH = 'files/CIS_Microsoft_365_Foundations_Benchmark_v3.0.0.pdf'
tables = tabula.read_pdf(PDF_PATH, pages=28, lattice=True)
# json_data = tabula.convert_into(tables[0], "json")
if tables:
    print(tables[0])
else:
    print("Failed to find tables")