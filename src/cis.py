import pdftotree
from bs4 import BeautifulSoup
import re
import requests
import json

# PDF_PATH = 'files/CIS_Microsoft_365_Foundations_Benchmark_v2.0.0.pdf'
PDF_PATH = 'files/CIS_Google_Workspace_Foundations_Benchmark_v1.1.0.pdf'
HTML_PATH = 'output/output.html'
QE_PORTAL = 'http://localhost:8000/api'
app_pattern = re.compile(r'^(\d+) (.+)$')
category_pattern = re.compile(r'^(\d+\.\d+) (.+)$')
recomm_pattern = re.compile(r'^(\d+\.\d+\.\d+) \((L\d)\) (.+?)')
RECOMM_START_PATTERN = re.compile(r'^(\d+(\.\d+)*)\s+\(L\d+\)\s+(.+?)$')
RECOMM_PATTERN = re.compile(r'^(\d+(\.\d+)*)\s+\(L(\d+)\)\s+(.+?)\s+(\((Manual|Automated)\))$')
RECOMM_PATTERN_2 = re.compile(r'^(\d+(\.\d+)*)\s+\(L(\d+)\)\s+(.+?)')
PAGE_NUM = re.compile(r'^Page\s([1-9]|[1-9]\d|[1-9]\d\d)$')

class Application:
    def __init__(
            self,
            number = "",
            name = ""
    ):
        self.number = number
        self.name = name
        self.categories = []
    
    def __str__(self):
        repr_str = f"{self.number} {self.name}"
        for s in self.categories:
            repr_str += f"\n\t{s}"
        return repr_str

class Category:
    def __init__(
            self,
            number = "",
            name = ""
    ):
        self.number = number
        self.name = name
        self.recommendations = []
    
    def __str__(self):
        repr_str = f"{self.number} {self.name}"
        for s in self.recommendations:
            repr_str += f"\n\t\t{s}"
        return repr_str

class CISRecommendation:
    def __init__(
            self,
            platform = "",
            category = "",
            ref_id = "",
            level = "",
            name = "",
            automated = False,
            profile_appllicability_license = "",
            profile_appllicability_level = "",
            description = "",
            rationale = "",
            impact = "",
            audit = "",
            remediation = "",
            default_value = "",
            references = "",
            cis_controls = ""
        ):
        self.platform = platform,
        self.category = category,
        self.ref_id = ref_id,
        self.level = level,
        self.name = name,
        self.automated = False,
        self.profile_appllicability_license = profile_appllicability_license
        self.profile_appllicability_level = profile_appllicability_level
        self.description = description
        self.rationale = rationale
        self.impact = impact
        self.audit = audit
        self.remediation = remediation
        self.default_value = default_value
        self.references = references
        self.cis_controls = cis_controls
    
    def __str__(self):
        return f"{self.ref_id} (L{self.level}) {self.name} ({'Automated' if self.automated else 'Manual'})\n\t{self.description}"

    def to_dict(self):
        return {
            "ref_id": self.ref_id,
            "name": self.name,
            "level": self.level,
            "automated": self.automated,
            "description": self.description
        }
    
class CIS_PARSER:
    def __init__(self, standard_name, standard_version):
        self.standard_name = standard_name
        self.standard_version = standard_version
        self.main_content_start = False
        self.current_recomm = None
        self.current_recomm_2 = None
        self.current_recomm_id = None
        self.current_content = None
        self.current_content_type = None
        self.recommendations = []

    def pdf_to_html(self, pdf_path):
        pdftotree.parse(pdf_path, html_path=HTML_PATH)

    def _get_text(self, node):
        raw_text = node.get_text().strip()
        text_list = raw_text.split("\n")
        line_text = " ".join(text_list)
        return line_text

    def _start_content(self, text, content_type, exclude_title=False):
        if not exclude_title:
            self.current_content = text
        else:
            self.current_content = ""
        self.current_content_type = content_type
    
    def _continue_content(self, text):
        if self.current_content is not None and self.current_content_type:
            self.current_content += f" {text}"

    def _close_content(self, content_type):
        if content_type == "recommendation_title":
            if not RECOMM_PATTERN.match(self.current_content):
                raise ValueError(f"Malformed content. Type: {content_type}")
            else:
                self.current_recomm.ref_id = RECOMM_PATTERN.match(self.current_content)[1]
                self.current_recomm.level = RECOMM_PATTERN.match(self.current_content)[3]
                self.current_recomm.name = RECOMM_PATTERN.match(self.current_content)[4]
                self.current_recomm.automated = RECOMM_PATTERN.match(self.current_content)[6] == "Automated"
        else:
            setattr(self.current_recomm, content_type, self.current_content)
        self.current_content_type = None
        self.current_content = None

    def _close_recom(self):
        if self.current_recomm:
            self.recommendations.append(self.current_recomm)
        self.current_recomm = None

    def _close_recom_2(self, recom_id):
        if self.current_recomm_2:
            with open(f'output/cis_msft_365_3_0_0/cis_msft_365_v3.0.0_control_{recom_id}.txt', 'a', encoding='utf-8') as file:
                file.write(f"This document talks about the details of Obsidian Security control (or CIS Recommendation) {self.current_recomm_id} in CIS Microsoft 365 Foundations Benchmark V3.0.0.\n")
                lines_with_newlines = [line + '\n' for line in self.current_recomm_2]
                file.writelines(lines_with_newlines)
        self.current_recomm_2 = None
    
    def _start_recom(self, text):
        if not self.current_recomm:
            self.current_recomm = CISRecommendation()
            self._start_content(text, "recommendation_title")
        else:
            self._close_recom()
            self._start_recom(text)
    
    def _start_recom_2(self, text, recom_id=None):
        
        if not self.current_recomm_2:
            self.current_recomm_id = RECOMM_START_PATTERN.match(text)[1]
            self.current_recomm_2 = []
            self.current_recomm_2.append(text)
        else:
            self._close_recom_2(recom_id)
            self._start_recom_2(text)

    def _close_cat(self):
        if self.current_cateogory:
            self.current_app.categories.append(self.current_cateogory)
        self.current_cateogory = None

    def _start_cat(self, text):
        if not self.current_cateogory:
            self.current_cateogory = Category(
                number=category_pattern.match(text).group(1),
                name=category_pattern.match(text).group(2)
            )
        else:
            self._close_recom()
            self._close_cat()
            self._start_cat(text)

    def _close_app(self):
        if self.current_app:
            self.applications.append(self.current_app)
        self.current_app = None

    def _start_app(self, text):
        if not self.current_app:
            self.current_app = Application(
                number=app_pattern.match(text).group(1),
                name=app_pattern.match(text).group(2)
            )
        else:
            self._close_cat()
            self._close_app()
            self._start_app(text)

    def parse_section_header(self, block):
        text = self._get_text(block)
        if text == "Recommendations":
            self.main_content_start = True
        if not self.main_content_start:
            return True
        
        if text == "CIS Controls:":
            # If it encounters this label it means the rest is a table
            return True
        elif text == "Profile Applicability:":
            self._close_content("recommendation_title")
        elif text == "Description:":
            self._start_content(text, "description", exclude_title=True)
        elif text == "Rationale:":
            self._close_content("description")

        elif RECOMM_START_PATTERN.match(text):
            self._start_recom(text)
        else:
            self._continue_content(text)
        return False

    def parse_section_header_2(self, block):
        text = self._get_text(block)
        if PAGE_NUM.match(text):
            return False
        if text == "Recommendations":
            self.main_content_start = True
            return True
        if not self.main_content_start:
            return True
        
        if text == "CIS Controls:":
            # If it encounters this label it means the rest is a table
            return True
        elif RECOMM_START_PATTERN.match(text):
            self._start_recom_2(text, self.current_recomm_id)
        else:
            if self.current_recomm_2:
                self.current_recomm_2.append(text)
        
        return False

    def parse_paragraph(self, block):
        if not self.main_content_start:
            return
        else:
            text = self._get_text(block)
            self._continue_content(text)
        
    def parse_header(self, block):
        text = self._get_text(block)
        if text == "Recommendations":
            self.main_content_start = True
        elif app_pattern.match(text):
            if self.current_app:
                self.applications.append(self.current_app)
            self.current_app = Application(
                number=app_pattern.match(text).group(1),
                name=app_pattern.match(text).group(2)
            )     

    def parse_page(self, page):
        blocks = page.select(".ocrx_block")
        for block in blocks:
            section_type = block['pdftotree']
            if section_type == 'section_header' or section_type == 'header':
                skip_page = self.parse_section_header(block)
                if skip_page:
                    return
            elif section_type == 'paragraph':
                self.parse_paragraph(block)

    def parse_page_2(self, page):
        blocks = page.select(".ocrx_block")
        for block in blocks:
            section_type = block['pdftotree']
            if section_type == 'section_header' or section_type == 'header' or section_type == 'paragraph':
                skip_page = self.parse_section_header_2(block)
                if skip_page:
                    return

    def html_to_text_files(self):
        with open(HTML_PATH, 'r', encoding='utf-8') as file:
            html_content = file.read()
        soup = BeautifulSoup(html_content, 'html.parser')
        pages = [child for child in soup.body.children if child.name == 'div']
        pn = 0
        for page in pages:
            pn += 1
            if pn==17:
                pass
            print(f"Processing page {pn}")
            self.parse_page_2(page)
        self._close_recom_2(self.current_recomm_id)

    def html_to_json(self):
        with open(HTML_PATH, 'r', encoding='utf-8') as file:
            html_content = file.read()

        soup = BeautifulSoup(html_content, 'html.parser')
        pages = [child for child in soup.body.children if child.name == 'div']
        pn = 0
        for page in pages:
            pn += 1
            print(f"Processing page {pn}")
            self.parse_page(page)
        self._close_recom()

    def save_controls(self):
        controls = []
        for rec in self.recommendations:
            controls.append(rec.to_dict())
        # save to local json
        with open('output/controls.json', 'w') as file:
            json.dump(controls, file, indent=4)
        payload_dict = {"standard": self.standard_name, "version": self.standard_version}
        payload_dict["controls"] = controls

        url = f"{QE_PORTAL}/savePostureControls"
        headers = {
            "Content-Type": "application/json"
        }

        response = requests.post(url, json=json.dumps(payload_dict), headers=headers)
        return response.text
        