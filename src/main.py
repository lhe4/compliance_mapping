import click
import json
import os
from cis import CIS_PARSER
from mapping import get_ai_rule_mapping_message, get_ai_setting_mapping_message, load_control_rule_list, parse_mappings, load_obsidian_settings 
from publish_helper import create_update_standard, get_standard, save_settings, load_settings

@click.group()
def run():
    pass

@click.command
def import_settings():
    settings = load_obsidian_settings()
    res = save_settings(settings)
    click.echo(res)

@click.command
@click.option('--standard', required=True, type=str, help='The name of the standard, for example, "cis_google_workspace_benchmark"')
@click.option('--standard-version', required=True, type=str, help='The version of the standard, for example, "1.0.0"')
@click.option('--platform', required=False, type=str, help='The impacted platform, for example, "microsoft"')
@click.option('--standard-file', required=False, type=str, help='The standard file name, for example, "CIS_Microsoft_365_Foundations_Benchmark_v3.0.0.pdf". display_name is used by default if the value is not given.')
def create_mappings(standard, standard_version, platform, standard_file):
    
    # Load standard information
    click.echo(f"Checking standard {standard}, version {standard_version}......")
    res, msg = get_standard(standard, standard_version)
    if not res:
        click.echo(click.style(f"Error: {msg}", fg="red"), err=True)
        return
    standard_details = json.loads(msg)
    click.echo(f"Standard is retrieved: {msg}")
    standard_type = standard_details["type"]
    if standard_type not in ['cis_benchmark']:
        click.echo(click.style(f"Error: standard not supported: {standard_type}", fg="red"), err=True)
        return
    
    # Read Standard PDF file
    if not standard_file:
        click.echo(f"Standard file name is not specified, using standard display name as default: {standard_details['display_name']}")
        standard_file = standard_details['display_name'] + ".pdf"
    standard_file_path = f"files/{standard_file}"      
    if not os.path.exists(standard_file_path):
        click.echo(click.style(f"Error: Standard file not found: {standard_file_path}", fg="red"), err=True)
        return
    # click.echo("Now converting PDF file to html file for further processing...")
    parser = CIS_PARSER(
        standard_name=standard,
        standard_version=standard_version
    )
    parser.pdf_to_html(standard_file_path)
    
    click.echo("Now extracting controls from html file...")
    parser.html_to_json()
    parser.html_to_text_files()
    click.echo("Saving controls...")
    response_txt = parser.save_controls()
    click.echo(response_txt)
    platform_id = standard_details['platform'].lower()
    click.echo(f"Now we are loading rules for platform_id: {platform_id}...")
    controls, rules = load_control_rule_list(platform_id)
    click.echo(f"Now we are reading mapping results from LLM service...")
    get_ai_rule_mapping_message(controls=controls, rules=rules)
    settings = load_settings(platform_id)
    get_ai_setting_mapping_message(controls=controls, settings=settings)

@click.command()
@click.option('--type', required=True, type=str, help='The type of the standard, for example, "cis_benchmark"')
@click.option('--name', required=True, type=str, help='The name of the standard, for example, "cis_google_workspace_benchmark"')
@click.option('--platforms', required=True, type=str, help='The impacted platforms of the standard, separated by commas for example, "Microsoft,Google"')
@click.option('--version', required=True, type=str, help='The version of the standard, for example, "1.0.0"')
@click.option('--display-name', required=False, type=str, help='The display name of the standard, for example, "CIS Google Workspace Foundation Bench mark V1.0.0. If not provided it will use name value by default."')
def create_standard(type, name, platforms, version, display_name):
    if not display_name:
        display_name = name
    res, message = create_update_standard(
        name=name,
        platform=platforms,
        type=type,
        version=version,
        display_name=display_name
    )
    if res:
        click.echo(click.style(f"Standard {name} is created successfully!", fg="green"))
    else:
        click.echo(click.style(f"Error: {message}", fg="red"), err=True)

run.add_command(create_standard)
run.add_command(create_mappings)
run.add_command(import_settings)
if __name__ == '__main__':
    run()