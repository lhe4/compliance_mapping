import json
def output_rules(platform_id):
    with open("files/rules.json") as f:
        rules = json.load(f)
    filter_rules = []
    for r in rules.get("data").get("listGlobalPostureRules").get("rules"):
        if r.get("platform_id") == platform_id and r.get("obsidian_rule"):
            filter_rules.append({
                "rule_id": r.get("rule_id"),
                "name": r.get("name"),
                "description": r.get("description"),
                })
    with open(f"files/{platform_id}_rules.json", 'w') as f:
        json.dump(filter_rules, f)

def output_settings(platform_id):
    with open("files/settings.json") as f:
        settings = json.load(f)
    filter_settings = []
    for r in settings.get("data").get("listPostureSettings").get("data"):
    
        filter_settings.append({
            "setting_id": r.get("id"),
            "name": r.get("name"),
            "description": r.get("description"),
            })
    with open(f"files/{platform_id}_settings.json", 'w') as f:
        json.dump(filter_settings, f)
