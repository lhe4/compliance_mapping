[
  {
    "setting_id": "microsoftExchange - AtpPolicyForO365AllowSafeDocsOpen",
    "name": "Allow people to click through Protected View even if Safe Documents identified the file as malicious",
    "description": "This setting specifies whether users can click\nthrough and bypass the Protected View container even when Safe Documents identifies\na file as malicious.\n\nValid values are:\n\n- True: Users are allowed to exit the Protected\n  View container even if the document has been identified as malicious.\n- False: Users aren't allowed to exit Protected View in case of a malicious detection.\n"
  },
  {
    "setting_id": "microsoftTeams - CsTenantFederationConfigurationAllowTeamsConsumer",
    "name": "People in my org can communicate with Teams users whose accounts aren't managed by an organization",
    "description": "You can choose to enable or disable chat with external unmanaged Teams\nusers (those not managed by an organization, such as Microsoft Teams (free)).\nObsidian recommends you do not allow unmanaged Teams users to contact your organization, as this opens up\nyour organization to be phished by anyone with a Teams account.\n\n\nMeetings are not supported with unmanaged Teams users. If invited to a meeting,\nthey're considered anonymous when joining.\n"
  },
  {
    "setting_id": "microsoftTeams - CsTeamsClientConfigurationAllowGuestUser",
    "name": "Allow guest user",
    "description": "Designates whether or not guest users in your organization will have access to the Teams client.  If True, guests in your tenant will be able to access the Teams client.  Note that this setting has a core dependency on Guest Access being enabled in your Office 365 tenant.  For more information on this topic, read Authorize Guest Access in Microsoft Teams: <https://learn.microsoft.com/microsoftteams/teams-dependencies>."
  },
  {
    "setting_id": "microsoftTeams - CsTenantFederationConfigurationAllowPublicUsers",
    "name": "Allow users in my organization to communicate with skype users.",
    "description": "This setting allows Teams users in your organization to chat with\nand call Skype users. Teams users can then search for and start a one-on-one text-only\nconversation or an audio/video call with Skype users and vice versa.\n\nUnless needed keep this setting off as this opens up your organization to be phished\nby anyone with a Skype account.\n\n\nMeetings are not supported with Skype users. If invited to a meeting, they're\nconsidered anonymous when joining.\n"
  },
  {
    "setting_id": "microsoftSharepoint - PnPTenantDefaultLinkPermission",
    "name": "Choose the permission that's selected by default for sharing links",
    "description": "Prevent users from unintentionally granting edit permissions to SharePoint resources they share with others. Restrict the default link permission in SharePoint to 'View'.\nThis will require users to explicitly grant edit permissions to the recipients they wish to share content with. Note that this is a tenant wide setting that will affect SharePoint and OneDrive.\n"
  },
  {
    "setting_id": "microsoftSharepoint - PnPTenantDefaultSharingLinkType",
    "name": "Choose the type of link that's selected by default when users share files and folders in SharePoint and OneDrive",
    "description": "Default links created in SharePoint should not be set to 'Anyone with the link'. The link type should be set to either 'Only people in your organization'\nor 'Specific people (only people the user specifies)'. This will help reduce the risk of an unauthorized user accessing your organization's data.\n\nNote that this is a tenant wide setting that will affect SharePoint and OneDrive.\n\n- Direct = Specific people (only the people the user specifies)\n- Internal = Only people in your organization\n- AnonymousAccess = Anyone with the link\n"
  },
  {
    "setting_id": "microsoftAzure AD - organizationisMultipleDataLocationsForServicesEnabled",
    "name": "Is multiple data locations for services enabled",
    "description": "Multi-Geo capabilities in OneDrive and SharePoint Online enable control of shared resources like SharePoint team sites and Microsoft 365 Group mailboxes stored at rest in a specified geo location.\nThis gives you the flexibility to choose the country or region where each employee\u2019s Office 365 data is stored at-rest. Which can help you meet your global data residency needs.\n\nWe recommend to only use this feature if you are aware of the full details.\n\nNote this needs to be enabled by Microsoft support and is only supported by some subscription plans.\n\n- True: organization is Multi-Geo enabled\n- False: organization is not Multi-Geo enabled\n- Null (default): Read-only.\n\nFor more information, see search OneDrive Online Multi-Geo.\n"
  },
  {
    "setting_id": "microsoftSharepoint - PnPTenantOneDriveStorageQuota",
    "name": "OneDrive storage quota",
    "description": "Sets a default OneDrive for Business storage quota for the tenant. It will be used for new OneDrive for Business sites created. A typical use will be to reduce the amount of storage associated with OneDrive for Business to a level below what the License entitles the users. For example, it could be used to set the quota to 10 gigabytes (GB) by default. If value is set to 0, the parameter will have no effect. If the value is set larger than the Maximum allowed OneDrive for Business quota, it will have no effect. Enter a value of at least 1. For 1 TB of storage, enter 1024. If you have a subscription that provides more than 1 TB of storage, you can enter a value up to 5120 (5 TB)."
  },
  {
    "setting_id": "microsoftAzure AD - organizationsecurityComplianceNotificationMails",
    "name": "Security compliance notification mails",
    "description": "Specifies the email addresses to which security and compliance notifications are sent.\n\n AzureAD Powershell command:\n - Set-AzureADTenantDetail -SecurityComplianceNotificationMails \"example@example.com\"\n"
  },
  {
    "setting_id": "microsoftAzure AD - organizationsecurityComplianceNotificationPhones",
    "name": "Security compliance notification phones",
    "description": "Specifies the phone numbers to which security and compliance notifications are sent.\n\n AzureAD Powershell command:\n - Set-AzureADTenantDetail -SecurityComplianceNotificationPhones <list of phone numbers>\n"
  },
  {
    "setting_id": "microsoftSharepoint - PnPBrowserIdleSignoutSignOutAfter",
    "name": "Sign out after",
    "description": "To prevent malicious actors from exploiting long expiring session tokens, a short time interval should be set for browser sessions.\n\nThis parameter is used to specify a time value for Get-PnPBrowserIdleSignOut parameters such as SignOutAfter.\n\nSetting values are in minutes:\n  - 240 min = 4 hours\n  - 480 min = 8 hours\n  - 1440 min = 24 hours\n"
  },
  {
    "setting_id": "microsoftAzure AD - authorizationPolicyuserConsentToApps",
    "name": "User consent for applications",
    "description": "Third party app integrations can present a variety of risks to an organization. To reduce this\nrisk, restrictions should limit the apps users are allowed to grant consent to.\nOrganizations should either require an admin for all apps or only allow users to grant\nconsent to apps from verified publishers with low impact permissions.\n\nWe recommend organizations classify application permissions of their choice as 'low risk'.\nUsers are then restricted to granting consent only to applications that have those\npermissions. Taking advantage of this feature can help prevent unexpected high risk\napplications from accessing your resources.\n"
  },
  {
    "setting_id": "microsoftAzure AD - authorizationPolicydefaultUserRolePermissions.allowedToCreateApps",
    "name": "Users can register applications",
    "description": "Indicates whether the default user role can create applications. By default, non-admin users are allowed to create custom-developed applications. The creation of apps should be restricted to users with the required administrator roles.\n"
  },
  {
    "setting_id": "microsoftTeams - CsTeamsMeetingPolicyAllowAnonymousUsersToJoinMeeting",
    "name": "Allow anonymous users to join meeting",
    "description": "Determines whether anonymous users can join the meetings that impacted users organize. Set this to True to allow anonymous users to join a meeting. Set this to False to prohibit them from joining a meeting."
  },
  {
    "setting_id": "microsoftTeams - CsTeamsMeetingPolicyAllowParticipantGiveRequestControl",
    "name": "Allow participant give request control",
    "description": "Determines whether participants can request or give control of screen sharing during meetings scheduled by this user. Set this to TRUE to allow the user to be able to give or request control. Set this to FALSE to prohibit the user from giving, requesting control in a meeting."
  },
  {
    "setting_id": "microsoftTeams - CsTenantFederationConfigurationAllowFederatedUsers",
    "name": "People in my org can communicate with Teams users whose accounts aren't managed by an organization",
    "description": "This setting, when enabled (default), allows your users to potentially communicate with users from external domains. This broadens the scope for collaboration but can also increase your vulnerability to potential security threats. When disabled, it enforces stricter communication boundaries, permitting only intra-domain communication. While this can substantially reduce the risk of external threats, it can also inhibit legitimate communication with external collaborators."
  },
  {
    "setting_id": "microsoftSharepoint - PnPTenantLegacyAuthProtocolsEnabled",
    "name": "Apps that don't use modern authentication",
    "description": "Multi-factor authentication can be circumvented if legacy authentication is enabled. This allows malicious actors to access SharePoint with only a\nusername and password. Be aware that this is an organization-level setting that will affect all users.\n\n- False: Block access\n- True: Allow access\n"
  },
  {
    "setting_id": "microsoftTeams - CsTenantFederationConfigurationChooseWhichExternalDomainsYourUsersHaveAccessTo",
    "name": "Choose which external domains your users have access to",
    "description": "Allowing external domains to communicate within your organization is dangerous due to phishing and other social engineered attacks that your organization is exposed to. You should only allow domains you trust to communicate within your organization. When external domains are allowed, users in your organization can chat, add users to meetings, and use audio-video conferencing with users in external organizations. By default, your organization can communicate with all external domains."
  },
  {
    "setting_id": "microsoftSharepoint - PnPTenantDisallowInfectedFileDownload",
    "name": "Disallow infected file download",
    "description": "Microsoft 365 has a malware detection system that scans uploaded files. By default, the files it identifies as\nmalware can be downloaded by users after they read a warning. To prevent potential misuse, the ability to download\nthese files should be blocked.\n\nTo prevent users from downloading detected malicious files. Run the following command after connecting to SharePoint Online PowerShell:\n  - Set-SPOTenant -DisallowInfectedFileDownload $true\n\n[Learn more here](https://learn.microsoft.com/en-us/microsoft-365/security/office-365-security/safe-attachments-for-spo-odfb-teams-configure?view=o365-worldwide#step-2-recommended-use-sharepoint-online-powershell-to-prevent-users-from-downloading-malicious-files).\n"
  },
  {
    "setting_id": "microsoftExchange - OrganizationConfigOAuth2ClientProfileEnabled",
    "name": "Modern authentication",
    "description": "Modern authentication allows more secure methods to be used such as conditional access and multi-factor authentication. When you turn on modern authentication, Outlook 2013 for Windows or later will require it to sign in to Exchange Online mailboxes. Tenants created before August of 2017 will have this feature disabled by default.\n"
  },
  {
    "setting_id": "microsoftExchange - AtpPolicyForO365EnableATPForSPOTeamsODB",
    "name": "Enable ATP for SharePoint, Teams, OneDrive",
    "description": "The EnableATPForSPOTeamsODB parameter enables or disables Safe Attachments\nfor SharePoint, OneDrive, and Microsoft Teams.\n\nValid values are:\n\n- True: Safe Attachments for SharePoint, OneDrive, and Microsoft Teams is enabled. SharePoint Online admins\ncan use the DisallowInfectedFileDownload parameter on the Set-SPOTenant cmdlet\nto control whether users are allowed to download files that are found to be malicious.\n- False: Safe Attachments for SharePoint, OneDrive, and Microsoft Teams is disabled.\nThis is the default value.\n"
  },
  {
    "setting_id": "microsoftSharepoint - PnPTenantFolderAnonymousLinkType",
    "name": "Folders",
    "description": "'Anyone with the link' permissions allows anonymous users to access content without needing to sign in.\nThese types of links should only be used for sharing public content with an unspecified audience.\nThe default permission for these folder links should be set to 'View'. This will require users to explicitly\ngrant edit permissions when creating 'Anyone' links for folders.\n\nFolder permissions also allow upload which may increase the threat dangerous files.\n\nNote that this is a tenant wide setting that will affect SharePoint and OneDrive.\n\n- Edit = View, edit, and upload\n"
  },
  {
    "setting_id": "microsoftExchange - AtpPolicyForO365EnableSafeDocs",
    "name": "Turn on Safe Documents for Office clients",
    "description": "This setting enables or disables Safe Documents in\norganizations with Microsoft 365 A5 or Microsoft 365 E5 Security licenses.\n\nValid values are:\n\n- True: Safe Documents is enabled and will upload user files to Microsoft\nDefender for Endpoint for scanning and verification.\n- False: Safe Documents is disabled. This is the default value.\n"
  },
  {
    "setting_id": "microsoftSharepoint - PnPTenantPreventExternalUsersFromResharing",
    "name": "Allow guest to share items they don't own",
    "description": "The owners of files, folders, and sites should specify the audience they wish to share with.\nIt's possible for users to share links with external users who can then reshare that content with another party.\nTo reduce the risk of accidental data exposure, external users should be prevented from resharing content they\ndo not own.\n\nNote that this is a tenant wide setting that will affect SharePoint and OneDrive.\n\n- False: Prevent external users from resharing\n- True: Do not prevent external users from resharing\n"
  },
  {
    "setting_id": "microsoftTeams - CsTeamsClientConfigurationContentPin",
    "name": "Content pin",
    "description": "This setting applies only to Skype for Business Online (not Microsoft\nTeams) and defines whether the user must provide a secondary form of authentication\nto access the meeting content from a resource device account. Meeting content\nis defined as files that are shared to the \"Content Bin\" - files that have been\nattached to the meeting.\n\nPossible Values:\n- NotRequired\n- RequiredOutsideScheduleMeeting\n- AlwaysRequired\n\nThe default value is RequiredOutsideScheduleMeeting.\n\n- Set-CsClientPolicy -Identity Global -ContentPin AlwaysRequired\n"
  },
  {
    "setting_id": "microsoftTeams - CsTenantFederationConfigurationAllowTeamsConsumerInbound",
    "name": "External users with Teams accounts not managed by an organization can contact users in my organization",
    "description": "This setting is dependent on another setting, 'People in my org can\ncommunicate with Teams users whose accounts aren't managed by an organization'.\n\n\nKeep this setting off so that unmanaged Teams users will not be able to search\nby email address to find users in your organization. All communications with unmanaged\nTeams users must be initiated by users in your organization.\n"
  },
  {
    "setting_id": "microsoftSharepoint - PnPTenantFileAnonymousLinkType",
    "name": "Files",
    "description": "'Anyone with the link' permissions allows anonymous users to access content without needing to sign in.\nThese types of links should only be used for sharing public content with an unspecified audience.\nThe default permission for these file links should be set to 'View'. This will require users to explicitly\ngrant edit permissions when creating 'Anyone with the link' files.\n\nNote that this is a tenant wide setting that will affect SharePoint and OneDrive.\n\n- Edit = View and edit\n"
  },
  {
    "setting_id": "microsoftSharepoint - PnPTenantIPAddressEnforcement",
    "name": "IP Address Enforcement",
    "description": "Allows access from network locations that are defined by an administrator. The values are True and False. The default value is False which\n means the setting is disabled. Before the IPAddressEnforcement parameter is set, make sure you add a valid IPv4 or IPv6 address to the IPAddressAllowList\n parameter.\n\n SharePoint Online PowerShell command:\n - Set-SPOTenant -IPAddressEnforcement <Boolean>\n"
  },
  {
    "setting_id": "microsoftTeams - CsTeamsEnhancedEncryptionPolicyMeetingEndToEndEncryption",
    "name": "Meeting end to end encryption",
    "description": "Determines whether end-to-end encrypted meetings are available in Teams (requires a Teams Premium license). Set this to DisabledUserOverride to allow users to schedule end-to-end encrypted meetings. Set this to Disabled to prohibit."
  },
  {
    "setting_id": "microsoftTeams - CsOnlineDialInConferencingTenantSettingsPinLength",
    "name": "Pin length",
    "description": "Specifies the number of digits in the automatically generated PINs.\nOrganizers can enter their PIN to start a meeting they scheduled if they join via phone and are the first person to join.\nThe minimum value is 4, the maximum is 12, and the default is 5. A user's PIN will only authenticate them as leaders for a meeting they scheduled.\nThe PIN of a user that did not schedule the meeting will not enable that user to lead the meeting."
  },
  {
    "setting_id": "microsoftSharepoint - PnPTenantRequireAnonymousLinksExpireInDays",
    "name": "These links must expire within this many days",
    "description": "Anonymous links in SharePoint should be set to expire in 30 days or less. Expiring these anonymous links will help reduce the risk of data leakage and data exfiltration scenarios.\n\nNote that this is a tenant wide setting that will affect SharePoint and OneDrive.\n\n- Value of 999: Setting not set and there is no global expiration date.\n"
  },
  {
    "setting_id": "microsoftTeams - CsTeamsGuestCallingConfigurationAllowPrivateCalling",
    "name": "Allow private calling for guest",
    "description": "Designates whether guests who have been enabled for Teams can use calling functionality. If False, guests cannot call."
  },
  {
    "setting_id": "microsoftTeams - CsTeamsEnhancedEncryptionPolicyCallingEndtoEndEncryptionEnabledType",
    "name": "Calling end to end encryption enabled type",
    "description": "Determines whether end-to-end encrypted calling is available for the user in Teams. Set this to DisabledUserOverride to allow user to turn on end-to-end encrypted calls. Set this to Disabled to prohibit."
  },
  {
    "setting_id": "microsoftTeams - CsTeamsClientConfigurationResourceAccountContentAccess",
    "name": "Resource account content access",
    "description": "Require a secondary form of authentication to access meeting content. Possible values: NoAccess, PartialAccess and FullAccess."
  },
  {
    "setting_id": "microsoftTeams - CsTenantFederationConfigurationAllowedDomains",
    "name": "Allowed Domains",
    "description": "This setting identifies the specific domains that users in your organization are permitted to interact with through Microsoft Teams. By correctly configuring this, you can confine your network's exposure to only approved, trusted domains. If this setting is improperly configured or excessively permissive, it can expose your organization to potential security threats, including unauthorized access, introduction of malware, or social engineering attacks by malicious external entities."
  },
  {
    "setting_id": "microsoftTeams - CsTenantFederationConfigurationBlockedDomains",
    "name": "Blocked Domains",
    "description": "This setting enables you to designate domains that users in your organization are not permitted to communicate with. If the AllowedDomains setting is configured as AllowAllKnownDomains, this setting acts as an 'exclusion list', denying communication with potentially harmful or untrusted domains. However, if the AllowedDomains setting is not configured as AllowAllKnownDomains, the 'exclusion list' is disregarded, and communication is restricted to domains explicitly listed in the allowed domains. Misuse or misunderstanding of this setting can inadvertently allow threat actors to exploit your Teams network, leading to potential data breaches or unauthorized access to sensitive data."
  },
  {
    "setting_id": "microsoftAzure AD - identitySecurityDefaultsEnforcementPolicySecurity Defaults",
    "name": "Security defaults",
    "description": "Security defaults are a single setting that protects your organization by automatically applying several preconfigured security measures. These measures include requiring multi-factor authentication for users and administrators and blocking outdated signing methods. It increases the security of environments that do not want to set up conditional access policies."
  }
]
